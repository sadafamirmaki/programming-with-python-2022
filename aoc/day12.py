from collections import deque

with open("day12.txt", "r") as file:
    data = file.read().strip()


def parse_grid(data):
    grid = [list(line) for line in data.split("\n")]
    list1 = []
    start = end = None
    for i, row in enumerate(grid):
        for j, v in enumerate(row):
            if v == "S":
                start = (i, j)
                list1.append((i, j))
                grid[i][j] = "a"
            elif v == "E":
                end = (i, j)
                grid[i][j] = "z"
            elif v == "a":
                list1.append((i, j))
    return grid, start, end, list1


def bfs(grid, starts_point, end_point):
    seen = set()
    q = deque([(start, 0) for start in starts_point])
    while q:
        position, distance = q.popleft()
        if position == end_point:
            return distance
        if position in seen:
            continue
        seen.add(position)
        x, y = position
        for dx, dy in ((0, 1), (1, 0), (0, -1), (-1, 0)):
            if (0 <= x + dx < len(grid) and 0 <= y + dy < len(grid[0]) and ord(grid[x + dx][y + dy]) - ord(grid[x][y]) <= 1):
                q.append(((x + dx, y + dy), distance + 1))
    return float("inf")  # positive infinity


GRID, START_POINT, END_POINT, A_LIST = parse_grid(data)


print(bfs(GRID, [START_POINT], END_POINT))  # Part 1
print(bfs(GRID, A_LIST, END_POINT))  # Part 2
#reddit helped me alot also one of my friends 