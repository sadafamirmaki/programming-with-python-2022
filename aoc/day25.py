def cal(inp):
    summation = 0
    for i, j in enumerate(inp[::-1]):
        if j == "=":
            j = -2
        if j == "-":
            j = -1
        summation += (int(j) * 5**i)

    return (summation)

z = 0
with open("day25.txt" , 'r') as text_file:
    lines = text_file.readlines() 
for i in lines:
    z += (cal(i.strip()))


d_to_s = {0: '0', 1: '1', 2: '2', 3: '=', 4: '-'}


def to_snafu(number):
    snafu = ''
    while number > 0:
        number, rem = divmod(number, 5)
        snafu += d_to_s[rem]
        if rem > 2:
            number += 1
    return snafu[::-1] if snafu else '0'


print("Part 1:", to_snafu(z))
 #used reddit and asked my friend about it