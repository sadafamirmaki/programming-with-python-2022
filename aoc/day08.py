#part one
with open("day08.txt", "r") as data:
    data = data.read().splitlines()
length=len(data)
width= len(data[0])
def visible_from_outside(r,c):
    current=data[r][c]  
    rules = [
        r==0 or r==length-1 or c==0 or c==width-1,
        all(data[i][c] < current for i in range(0,r)),
        all(data[i][c] < current for i in range(r+1, length)),
        all(data[r][j] < current for j in range(0, c)),
        all(data[r][j] < current for j in range(c+1, width )) ]
    return True if any(rules) else False
visible = sum(visible_from_outside(x,y) for x in range(length) for y in range(width))
print (visible)
#part two
from math import prod
def scenic(r,c):
    current = data[r][c]
    dirs= [(1,0),(-1,0),(0,1),(0,-1)]
    scenes=[]
    for (dy,dx) in dirs:
        line=0
        newR=r+dy
        newC=c+dx
        while not any([newR==-1 , newR==length , newC==width , newC==-1]):
            line+=1

            if data[newR][newC]>=current:
                break
            newR += dy
            newC += dx
        scenes.append(line)
    return prod(scenes)
m = max([scenic(x,y) for x in range(length) for y in range(width)])
print (m)
