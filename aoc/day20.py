def get_init(decryption_key=1):
    #used reddit and asked my friend about it
    data = open('day20.txt' , 'r').readlines()
    data = [line.strip() for line in data]
    numbers = [(i, int(x) * decryption_key) for i, x in enumerate(data)]
    original_numbers = [x[1] for x in numbers]
    return numbers, original_numbers

def mix(numbers, original_numbers): 
    for i, line in enumerate(original_numbers):
        number = int(line)
        tofind = (i, number)
        index = numbers.index(tofind)
        left_list = numbers[:index]
        right_list = numbers[index+1:]
        temp = left_list + right_list
        newpos = (index+number) % len(temp)
        if newpos == 0:
            temp.append(tofind)
        else:
            temp.insert(newpos, tofind)
        numbers = temp
    return numbers
def get_checksum(numbers):
    summed = 0
    indices = [1000, 2000, 3000]
    for index_zero, x in enumerate(numbers):
        if x[1] == 0:
            break    
    for i in indices:
        summed += numbers[(index_zero + i)%len(numbers)][1]
    return summed
# part 1
numbers, original = get_init(decryption_key=1)
numbers = mix(numbers, original)
print(get_checksum(numbers))
# part 2
numbers, original = get_init(decryption_key=811589153)
for i in range(10):
    shuffled = mix(numbers, original)
    numbers = shuffled
print(get_checksum(numbers))