#part one
with open("day06.txt", "rt") as f:
    lines = f.readlines()
result = []

for x in range(len(lines[0])-3):
    if lines[0][x] != lines[0][x + 1] and lines[0][x] != lines[0][x + 2] and lines[0][x] != lines[0][x + 3] and lines[0][x + 1] != lines[0][x + 2] and lines[0][x + 1] != lines[0][x + 3] and lines[0][x + 2] != lines[0][x + 3]:
        print(x+4)
        break

#part2

print([[i+14 for i in range(len(lines)) if len(set(lines[i:i+14])) == 14] for lines in [open("day06.txt").read()]][0][0])