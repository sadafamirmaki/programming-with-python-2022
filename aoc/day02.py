# part 1
with open("day02.txt", "rt") as f:
    lines = f.readlines()
result = []
result2 = []
for x in lines:
    result.append(x.rstrip("\n").split(' ')[1])
    result2.append(x.rstrip("\n").split(' ')[0])

f.close()

total = 0

for i in range(len(result)):

    if result2[i] == "A" and result[i] == "X":
        total += 4

    if result2[i] == "A" and result[i] == "Y":
        total += 8
    if result2[i] == "A" and result[i] == "Z":
        total += 3
    if result2[i] == "B" and result[i] == "X":
        total += 1
    if result2[i] == "B" and result[i] == "Y":
        total += 5
    if result2[i] == "B" and result[i] == "Z":
        total += 9
    if result2[i] == "C" and result[i] == "X":
        total += 7
    if result2[i] == "C" and result[i] == "Y":
        total += 2
    if result2[i] == "C" and result[i] == "Z":
        total += 6

print(total)

# Part 2


total2 = 0

for i in range(len(result)):

    if result2[i] == "A" and result[i] == "X":
        total2 += 3

    if result2[i] == "A" and result[i] == "Y":
        total2 += 4
    if result2[i] == "A" and result[i] == "Z":
        total2 += 8
    if result2[i] == "B" and result[i] == "X":
        total2 += 1
    if result2[i] == "B" and result[i] == "Y":
        total2 += 5
    if result2[i] == "B" and result[i] == "Z":
        total2 += 9
    if result2[i] == "C" and result[i] == "X":
        total2 += 2
    if result2[i] == "C" and result[i] == "Y":
        total2 += 6
    if result2[i] == "C" and result[i] == "Z":
        total2 += 7

print(total2)
