commands = []
with open("day09.txt" , 'r') as f:
    data = f.read().split("\n")
    for i in data :
        a = i.split(' ')
        commands.append(a)

direction = {"U":(0,1),"D":(0,-1),"L":(-1,0),"R":(1,0)}

def soloution(x):
    y = [[0, 0] for _ in range(x)]
    visited = set()
    visited.add((0,0))
    for command in commands:
        x,n = command
        for _ in range(int(n)):
            y[0][0] += direction[x][0]
            y[0][1] += direction[x][1]
            for i in range(1, len(y)):
                cx, cy = y[i-1][0] - y[i][0], y[i-1][1] - y[i][1]
                if max(abs(cx), abs(cy)) > 1:
                    y[i][0] += (1 if cx > 0 else 0 if cx == 0  else -1)
                    y[i][1] += (1 if cy > 0 else 0 if cy == 0 else -1)
            visited.add(tuple(y[-1]))
    return len(visited)

print(soloution(2))
print(soloution(10))