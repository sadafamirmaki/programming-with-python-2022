with open("day14.txt", "r") as file:
    data = file.read().strip()
data = open("day14.txt").read()
data = data.split("\n")

walls = set()
max_y = 0
for wall in data:
    wall = wall.split(" -> ")
    for i in range(1, len(wall)):
        x, y = wall[i].split(",")
        x = int(x)
        y = int(y)
        pre_x, pre_y = wall[i - 1].split(",")
        pre_x = int(pre_x)
        pre_y = int(pre_y)
        if y > max_y or pre_y > max_y:
            max_y = max(y, pre_y)
        if x == pre_x:
            for ty in range(min(y, pre_y), max(y, pre_y) + 1):
                walls.add((x, ty))
        else:
            for tx in range(min(x, pre_x), max(x, pre_x) + 1):
                walls.add((tx, y))
max_y += 2


def sand_simulator(p1):
    obs = walls.copy()
    while True:
        if not p1 and (500, 0) in obs:
            return len(obs) - len(walls)
        sand_x = 500
        sand_y = 0
        while True:
            if sand_y + 1 == max_y:
                if p1:
                    return len(obs) - len(walls)
                else:
                    obs.add((sand_x, sand_y))
                    break
            if (sand_x, sand_y + 1) not in obs:
                sand_y += 1
            elif (sand_x - 1, sand_y + 1) not in obs:
                sand_y += 1
                sand_x -= 1
            elif (sand_x + 1, sand_y + 1) not in obs:
                sand_y += 1
                sand_x += 1
            else:
                obs.add((sand_x, sand_y))
                break


# Part 1
print(sand_simulator(True))

# Part 2
print(sand_simulator(False))
##reddit helped me alot also my friend