# Part 1
with open("day10.txt", "r") as f:
    data = f.read().split('\n')
y = [-1]
x = 1
for i in data:
    if i == "noop":
        y.append(x)
    else:
        y.append(x)
        y.append(x)
        x += int(i.split(" ")[1])

strengths = 0
for i in range(20, 221, 40):
    strengths += y[i] * i
print(strengths)

# Part 2
for i in range(1, 241, 40):
    screen = ""
    for j in range(i, i + 40):        
        if abs(y[j] - (j - 1) % 40) < 2:
            screen += "#"
        else:
            screen += "."
    print(screen)
    #searched on the internet and used reddit