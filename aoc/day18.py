with open("day18.txt" , 'r') as f:
    data = f.readlines()

data = [tuple([int(y) for y in x.split(",")]) for x in data]
# part 1
highest_coord = 0
for coord in data:
    m = max(coord)
    if m > highest_coord:
        highest_coord = m

total1 = 0
for cube in data:
    add = 6
    for neighbor_tuple in [(1,0,0),(-1,0,0),(0,1,0),(0,-1,0), (0,0,1),(0,0,-1)]:
        check = tuple(map(lambda x, y: x + y, cube, neighbor_tuple))
        if check in data:
            add -= 1    
    total1 += add
print(total1)
# part 2
total2 = 0
queue = [(-highest_coord-1,-highest_coord-1,-highest_coord-1)]
seen = set()
while queue != []:
        c = queue.pop(0)
        if c not in data:
            for neighbor_tuple in [(1,0,0),(-1,0,0),(0,1,0),(0,-1,0), (0,0,1),(0,0,-1)]:
                check = tuple(map(lambda x, y: x + y, c, neighbor_tuple))
                if check in data:
                    total2 += 1
                else:
                    if not check in seen and check[0] <= highest_coord+2 and check[1] <= highest_coord+2 and check[2] <= highest_coord+2 and check[0] >= -highest_coord-2 and check[1] >= -highest_coord-2 and check[2] >= -highest_coord-2:
                        queue.append(check)
                        seen.add(check)


print(total2)
