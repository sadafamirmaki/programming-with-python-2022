#part 1
def parse(text):
    stacks = [""]*10
    for line in text[:-1]:
        for i, box in enumerate(line[1::4]):
            if box != " ": stacks[i+1] += box
    return stacks
    
data = open("day05.txt").read()
text, inst = [part.split("\n") for part in data.split("\n\n")]
stacks = parse(text)

a, b = stacks[:], stacks[:]
for line in inst:
    _, n, _, origin, _, dest = line.split()
    n = int(n); origin = int(origin); dest = int(dest)

    a[origin], a[dest] = a[origin][n:],  a[origin][:n][::-1] + a[dest]
#part 2
    b[origin], b[dest] = b[origin][n:],  b[origin][:n]       + b[dest]

print("Part 1:", "".join(s[0] for s in a if s))
print("Part 2:", "".join(s[0] for s in b if s))
#used stackoverflow and reddit for this question