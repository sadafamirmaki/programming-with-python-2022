#part one
import string 
with open ("day03.txt" , "rt") as f:
    data = f.readlines()
uppercase = list(string.ascii_uppercase)
lowercase = list(string.ascii_lowercase)
letters = lowercase + uppercase
total = 0
for i in data:
    i = i.strip()
    a = i[0:(len(i)//2)]
    b = i [(len(i)//2) :]
    for j in letters:
        if (j in a) and (j in b):
            x = letters.index(j) + 1
            total = total + x
print(total)

#part two
total2 = 0
composite_list = [data[x:x+3] for x in range(0, len(data),3)]
for i in composite_list:
    a = i[0].strip()
    b = i[1].strip()
    c = i[2].strip()
    for j in letters:
        if (j in a) and (j in b) and (j in c):
            y = letters.index(j) + 1
            total2 += y
print(total2)




